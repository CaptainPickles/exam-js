window.addEventListener("DOMContentLoaded"  , async() => {

    const reponse = await fetch("http://localhost:3000/depenses" )

    const depenses = await reponse.json(); 
    console.log(depenses)
    
    // document.querySelector(".js-list-tache").innerHTML = genererFormsTaches(depenses);
    document.querySelector(".table-content").innerHTML = genererFormsDepenses(depenses);

    // gestion du nombre de tâches en cours 
    document.querySelector(".js-compteur").innerHTML = getTotal(depenses); 
    document.querySelector(".js-compteur-depenses").innerHTML = getTotalDepenses(depenses); 
    document.querySelector(".js-compteur-recettes").innerHTML = getTotalRecettes(depenses); 


    // écouter quand on clique dans la zone table-content
    document.querySelector(".table-content").addEventListener("click" , async e => {
        e.preventDefault();
        if(e.target.className.includes("btn")){
            const form =  e.target.parentNode.parentNode.firstElementChild;
            console.log(form)
            const action = e.target.value ;
            console.log(action)
            const id = form.id
            console.log(form.test)
            if(action == "modifier"){
                const data = {
                    id : id,
                    name : form.name,
                    amount : form.amount.value
                }
                const options = { method : "PUT" , body : JSON.stringify(data) , headers : {'Content-Type': 'application/json'} }
                await fetch("http://localhost:3000/depenses/"+id , options)
            }else if(action == "supprimer"){
                const options = {method : "DELETE"}
                await fetch("http://localhost:3000/depenses/"+id , options);
            }
        }
    })
})

function getTotalRecettes(depenses){
    let result = 0
    if(!depenses || depenses.length === 0 ){
        return result
    }
    depenses.forEach(depense => {
        if( depense.amount < 0 ){
            result += depense.amount
        }
    });
    return result
}
function getTotalDepenses(depenses){
    let result = 0
    if(!depenses || depenses.length === 0 ){
        return result
    }
    depenses.forEach(depense => {
        if( depense.amount > 0 ){
            result += depense.amount
        }
    });
    return result
}

function getTotal(depenses){
    let result = 0
    if(!depenses || depenses.length === 0 ){
        return result
    }

    depenses.forEach(depense => {
        result = result + depense.amount
    });
    return result
}

function genererFormsDepenses(data){
    if(data.length === 0) return "<p>Veuillez ajouter des Dépenses</p>";

    return data.map( d => {
        return `<tr><form id="${d.id}" name="${d.name}" amount="${d.amout}">
        <td>${d.id}</td>
        <td> <input type="text" name="name" class="form-input" value="${d.name}"></td>
        <td> <input type="number" name="amount" class="form-input" value="${d.amount}"></td>
        <td>
        <input type="hidden" name="id" class="form-input" value="${d.id}">
        <input type="submit" class="btn btn-primary mx-3" refId="form${d.id}" value="modifier">
        <input type="submit" class="btn btn-danger" value="supprimer">
    </td>
    </form>
    </tr>
      `
    } ).join("")
}
